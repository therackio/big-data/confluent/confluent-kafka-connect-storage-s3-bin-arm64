ARG PARENT_IMAGE
FROM $PARENT_IMAGE

ARG CONFLUENT_CONNECT_STORAGE_S3_VERSION

RUN \
    set eux && \
    cd / && \
    git clone https://github.com/confluentinc/kafka-connect-storage-cloud && \
    cd kafka-connect-storage-cloud  && \   
    git fetch origin refs/tags/v$CONFLUENT_CONNECT_STORAGE_S3_VERSION:refs/tags/v$CONFLUENT_CONNECT_STORAGE_S3_VERSION && \
    git checkout tags/v$CONFLUENT_CONNECT_STORAGE_S3_VERSION && \
    mvn package -P standalone -DskipTests && \
    ls -al 